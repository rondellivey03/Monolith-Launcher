## This project is archived, and no-longer updated, please refer to the newest Monolith Launcher [here](https://github.com/rondellivey03/Monolith-Launcher-2)

![enter image description here](https://i.postimg.cc/G3q0FNk0/MONOLITH-LAUNCHER-IMG.png)
#

![enter image description here](https://img.shields.io/github/license/rondellivey03/Monolith-Launcher) ![enter image description here](https://img.shields.io/github/issues/rondellivey03/Monolith-Launcher) ![enter image description here](https://img.shields.io/github/forks/rondellivey03/Monolith-Launcher) 

Monolith Launcher is an application created by Rondell, it allows a user of the application to launcher directly into the Monolith Servers, whether it's their Garry's Mod or Rust, or any future server or game, the launcher also features several other things such as quick-links to different areas of the Monolith Website.

**Monolith Launcher Discord:** https://discord.gg/GR53CNQ
Join the Discord Server to discuss about the application, and also be notified of new commits / announcements regarding the application. You can also ask for help & questions here too.

**Question:** How can I make contributions to Monolith Launcher?

**Answer:** [Check this out by Marc Diethelm](https://github.com/MarcDiethelm/contributing/blob/master/README.md)

**If any problems occur message me on discord or create an issue**
My Discord Username is Rondell#5907 or [Click here to make an issue.](https://github.com/rondellivey03/Monolith-Launcher/issues/new/choose)

## Contribute!
You're welcome to making contributions to this project, your contributions to the application is what helps shape and make the application better, any contributions you make to the Monolith Launcher is always appreciated!

## Preview
![enter image description here](https://i.postimg.cc/prL2cLp1/1.png)


