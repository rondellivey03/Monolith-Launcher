---
name: Question
about: This is the required template to use when creating Questions regarding Monolith
  Launcher.
title: "[Question] Title Here"
labels: Question
assignees: rondellivey03

---

**What's your question?:**

**Did you check F.A.Q on our Discord?:** [E.g. Yes, No]

**If not, here's the Discord.:** https://discord.gg/Bfh5hUc
