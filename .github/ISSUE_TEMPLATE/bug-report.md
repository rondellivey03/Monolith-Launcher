---
name: Bug Report
about: This is the required template to use when creating Bug Reports regarding Monolith
  Launcher.
title: "[BUG Report]: Title Here"
labels: bug report
assignees: rondellivey03

---

**Priority:** [E.g. Low, Medium, High]

**How can I reproduce issue?:** 

**Describe the Issue:**

**Do you have an error log? if so, please include it.**

**Additional Information:**
