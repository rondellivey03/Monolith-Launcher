---
name: Feedback
about: Wanna leave feedback about how much you love the application? Choose this template!
title: "[Feedback] Feedback about Monolith Launcher"
labels: feedback
assignees: rondellivey03

---

**What's your discord name?:** [E.g. Name#1234] 

**What do you rate the application?:** [E.g. 1 - 5] 

**What feedback do you have about the application?:** 

**Any and all additional information, add here:**
