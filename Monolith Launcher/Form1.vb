﻿Imports System.Media
Imports HtmlAgilityPack

Public Class MLauncher

    Public Class CustomRenderer
        Inherits ToolStripSystemRenderer

        Protected Overrides Sub OnRenderToolStripBorder(ByVal e As System.Windows.Forms.ToolStripRenderEventArgs)
            'Do nothing
        End Sub

    End Class

    Dim thread As String = My.Settings.threadtimer
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles XLink.LinkClicked
        Me.Close()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles MiniLink.LinkClicked
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub MLauncher_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.Settings.whosonline = "True" Then
            Try
                Dim url As String = "https://gmod-servers.com/server/61805/"
                Dim web As New HtmlWeb()
                Dim doc As HtmlDocument = web.Load(url)
                Dim names As HtmlNodeCollection = doc.DocumentNode.SelectNodes("/html/body/div[3]/div/div[4]/div/div[5]/div/div")
                Threading.Thread.Sleep(thread)
                For Each AnyWantedWord In names
                    Dim str As String = ","
                    Dim mtr As String = vbCrLf
                    RichTextBox2.Text = (AnyWantedWord.InnerText)
                    RichTextBox2.Text = RichTextBox2.Text.Replace(",", vbNewLine)
                Next
            Catch ex As Exception
                RichTextBox2.Text = "Monolith Launcher was unable to retrieve player-list."
            End Try
        Else
            Panel9.Visible = False
        End If
        Label3.Text = "Version " + Application.ProductVersion + " - Branch " + My.Settings.gitbranch
        Version1020000ToolStripMenuItem.Text = "Version " + Application.ProductVersion
        If My.Computer.Network.IsAvailable = False Then
            Panel7.Dock = DockStyle.Fill
            Panel7.Show()
        End If
        If My.Settings.whatsnew = "True" Then
            whatsnew.Show()
        End If

    End Sub
    Private Sub PROCESSCHK_Tick(sender As Object, e As EventArgs) Handles PROCESSCHK.Tick
        Dim p() As Process
        p = Process.GetProcessesByName("hl2")
        If p.Count > 0 Then
            GameInfo.Show()
        Else
            GameInfo.Hide()
        End If
    End Sub
    Private Sub LinkLabel6_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel6.LinkClicked
        Process.Start("https://monolithservers.com/forums/")
    End Sub

    Private Sub LinkLabel5_LinkClicked_1(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        Process.Start("https://monolithservers.com/store")
    End Sub

    Private Sub LinkLabel7_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel7.LinkClicked
        Process.Start("https://monolithservers.com/servers")
    End Sub

    Private Sub LinkLabel10_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel10.LinkClicked
        Process.Start("https://discord.gg/uj6NRBS")
    End Sub

    Private Sub LinkLabel9_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel9.LinkClicked
        Process.Start("http://discord.gg/YQcjJtD")
    End Sub

    Private Sub LinkLabel8_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel8.LinkClicked
        Process.Start("https://steamcommunity.com/groups/MonolithServers")
    End Sub

    Private Sub LinkLabel13_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel13.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/announcements.4/")
    End Sub

    Private Sub LinkLabel12_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel12.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/rules-information.5/")
    End Sub

    Private Sub LinkLabel11_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel11.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/updates.43/")
    End Sub

    Private Sub LinkLabel15_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel15.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/player-reports.33/")
    End Sub

    Private Sub LinkLabel14_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel14.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/staff-reports.37/")
    End Sub

    Private Sub LinkLabel16_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel16.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/refund-requests.25/")
    End Sub

    Private Sub LinkLabel17_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel17.LinkClicked
        Process.Start("https://monolithservers.com/forums/forums/appeals.48/")
    End Sub
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        BunifuTransition1.HideSync(ErrCard)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Application.Exit()
    End Sub
    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click
        My.Settings.whatsnew = "True"
        My.Settings.Save()
        My.Settings.Reload()
        BunifuTransition1.ShowSync(whatsnew)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If My.Computer.Network.IsAvailable = True Then
            ChkUpd.Show()
            Me.Close()
        Else
            MsgBox("No internet connection found.")
        End If
    End Sub

    Private Sub MLauncher_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        TrayIcon.Dispose()
    End Sub
    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Settings.Show()
    End Sub
    Private Sub LinkLabel3_LinkClicked_1(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        RichTextBox2.Text = "Refreshing.. "
        If My.Settings.whosonline = "True" Then
            Try
                Dim url As String = "https://gmod-servers.com/server/61805/"
                Dim web As New HtmlWeb()
                Dim doc As HtmlDocument = web.Load(url)
                Dim names As HtmlNodeCollection = doc.DocumentNode.SelectNodes("/html/body/div[3]/div/div[4]/div/div[5]/div/div")
                Threading.Thread.Sleep(thread)
                For Each AnyWantedWord In names
                    Dim str As String = ","
                    Dim mtr As String = vbCrLf
                    RichTextBox2.Text = (AnyWantedWord.InnerText)
                    RichTextBox2.Text = RichTextBox2.Text.Replace(",", vbNewLine)
                Next
            Catch ex As Exception
                RichTextBox2.Text = "Monolith Launcher was unable to retrieve player-list."
            End Try
        Else
            RichTextBox2.Text = "Whos Online was disabled in Advanced Settings."
        End If
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Process.Start("https://discord.gg/GR53CNQ")
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Process.Start("https://github.com/rondellivey03/Monolith-Launcher/issues")
    End Sub
    Private Sub PictureBox12_Click(sender As Object, e As EventArgs) Handles PictureBox12.Click
        Process.Start("https://monolithservers.com/forums/members/rondell.6973/")
    End Sub
    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click

    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        My.Settings.whatsnew = "False"
        My.Settings.Save()
        My.Settings.Reload()
        BunifuTransition1.HideSync(whatsnew)
    End Sub
    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Me.Controls.Clear() 'removes all the controls on the form
        InitializeComponent() 'load all the controls again
        MLauncher_Load(e, e) 'Load everything in your form load event again
    End Sub
    Private Sub BunifuFlatButton2_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton2.Click
        If My.Settings.menu_expand = "True" Then
            Panel3.Width = "51"
            My.Settings.menu_expand = "False"
        ElseIf My.Settings.menu_expand = "False" Then
            Panel3.Width = "189"
            My.Settings.menu_expand = "True"
        End If
    End Sub
    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim p() As Process
        p = Process.GetProcessesByName("hl2")
        If p.Count > 0 Then
            ErrCard.Dock = DockStyle.Fill
            BunifuTransition1.ShowSync(ErrCard)
            ErrDes.Text = ("hl2.exe appears to already be opened." + vbNewLine + vbNewLine + "Is hl2.exe not opened? - Right click your Taskbar then open Task Manager, Extend more details, Check and see if there's a instance of hl2.exe running, if so, terminate it." + vbNewLine + vbNewLine + "If error still presists, Contact Rondell#5907 on Discord for help.")
            SystemSounds.Hand.Play()
        Else
            Process.Start("steam://connect/208.103.169.58:27015")
        End If
    End Sub
    Private Sub BunifuFlatButton3_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton3.Click
        Process.Start("https://steamcommunity.com/sharedfiles/filedetails/?id=1861050128")
    End Sub

    Private Sub BunifuFlatButton4_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton4.Click
        Settings.Show()
    End Sub
End Class