﻿Imports System.Net
Imports System.IO
Imports System.ComponentModel


Public Class ChkUpd
    Private InstalledVersion As String = Application.ProductVersion
    Private UpdateLink As String = "https://dl.dropboxusercontent.com/s/calgoceqcvgktyr/VersionTxt.txt?dl=0"
    Private UpToDate As String = "UpToDate"
    Private Outdated As String = "Outdated"
    Private Except As String = "Exception"
    Private LatestVersion As String
    Private MAJ As New WebClient
    Private Sub UpdateCheck_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles UpdateCheck.DoWork
        Try
            LatestVersion = MAJ.DownloadString(UpdateLink)
            If LatestVersion.Contains(InstalledVersion) Then
                e.Result = UpToDate
            Else
                e.Result = Outdated
            End If

            MAJ.Dispose()
        Catch ex As Exception
            e.Result = Except
        End Try
    End Sub
    Private Sub ChkUpd_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Label2.Text = "Branch: " + My.Settings.gitbranch
        If My.Computer.Network.IsAvailable = True Then
            ReqFiles.RunWorkerAsync()
        Else
            MLauncher.Show()
            Me.Close()
        End If
    End Sub
    Private Sub UpdateCheck_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles UpdateCheck.RunWorkerCompleted
        Try
            Select Case e.Result
                Case UpToDate
                    MLauncher.Show()
                    Me.Close()
                    'MonoUpdaterChk.RunWorkerAsync()
                Case Outdated
                    Process.Start(Application.StartupPath + "\MonoUpdater.exe")
                    Me.Close()
                Case Except
                    MLauncher.Show()
                    MLauncher.ErrCard.Show()
                    MLauncher.ErrCard.Dock = DockStyle.Fill
                    MLauncher.ErrDes.Text = ("Error during Update Check. This may be a simple issue with your internet connection." + vbNewLine + vbNewLine + "The launcher wasn't able to get an connection to check for updates, ERR_32-45, ERR_57-75, send this to me on Discord for help if this issue presists.")
                    Me.Close()
            End Select
        Catch ex As Exception
            MsgBox("Error during Update Check. This may be a simple issue with your internet connection." + vbNewLine + vbNewLine + ex.Message)
        End Try
    End Sub
    Private Sub XLink_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles XLink.LinkClicked
        Me.Close()
    End Sub
    Private Sub ReqFiles_DoWork(sender As Object, e As DoWorkEventArgs) Handles ReqFiles.DoWork
        Try
            If File.Exists(Application.StartupPath + "\HtmlAgilityPack.dll") Then
                'Checked
            Else
                Dim ReqDL1 As New WebClient
                Label1.Text = "Downloading HtmlAgilityPack.dll"
                ReqDL1.DownloadFile("https://dl.dropboxusercontent.com/s/07gmhj0373xe3uo/HtmlAgilityPack.dll?dl=1", Application.StartupPath + "\HtmlAgilityPack.dll")
                ReqDL1.Dispose()
            End If
            If File.Exists(Application.StartupPath + "\HtmlAgilityPack.pdb") Then
                'Checked
            Else
                Dim reqdl2 As New WebClient
                Label1.Text = "Downloading HtmlAgilityPack.pdb"
                reqdl2.DownloadFile("https://dl.dropboxusercontent.com/s/pbrfgfph4uh5g5z/HtmlAgilityPack.pdb?dl=1", Application.StartupPath + "\HtmlAgilityPack.pdb")
                reqdl2.Dispose()
            End If
            If File.Exists(Application.StartupPath + "\HtmlAgilityPack.xml") Then
                'Checked
            Else
                Dim reqdl3 As New WebClient
                Label1.Text = "Downloading HtmlAgilityPack.xml"
                reqdl3.DownloadFile("https://dl.dropboxusercontent.com/s/karrkrn2i3xjag1/HtmlAgilityPack.xml?dl=1", Application.StartupPath + "\HtmlAgilityPack.xml")
                reqdl3.Dispose()
            End If
            If File.Exists(Application.StartupPath + "\Monolith_Launcher_Fix.exe") Then
                'Checked
            Else
                Dim reqdl4 As New WebClient
                Label1.Text = "Downloading Monolith_Launcher_Fix.exe"
                reqdl4.DownloadFile("https://dl.dropboxusercontent.com/s/uvgkzwpqtguc1t9/Monolith_Launcher_Fix_v2.exe?dl=1", Application.StartupPath + "\Monolith_Launcher_Fix.exe")
                reqdl4.Dispose()
            End If
        Catch ex As Exception
            MsgBox("Error during File Check. This may be a simple issue with your internet connection." + vbNewLine + vbNewLine + ex.Message)
        End Try
    End Sub
    Private Sub ReqFiles_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles ReqFiles.RunWorkerCompleted
        Try
            UpdateCheck.RunWorkerAsync()
            Label1.Text = "Checking for Updates"
        Catch ex As Exception
            MsgBox("Error during File Check." + vbNewLine + vbNewLine + ex.Message)
        End Try

    End Sub
End Class