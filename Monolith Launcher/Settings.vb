﻿Imports System.IO

Public Class Settings
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim not1 As Integer
        not1 = MsgBox("You're about to erase your Garry's Mod addons, this doesn't unsubscribe you from your current subscribed addons, do you wanna continue?", vbYesNo)
        If not1 = vbYes Then
            Try
                'steamapps\common\GarrysMod\hl2.exe
                Dim strSteamInstallPath As String = My.Computer.Registry.GetValue(
           "HKEY_CURRENT_USER\Software\Valve\Steam", "InstallPath", Nothing)
                If Directory.Exists(strSteamInstallPath) Then
                    If Directory.Exists(strSteamInstallPath + "\steamapps\common\GarrysMod\garrysmod\addons") Then
                        Directory.Delete(strSteamInstallPath + "\steamapps\common\GarrysMod\garrysmod\addons", True)
                        Directory.CreateDirectory(strSteamInstallPath + "\steamapps\common\GarrysMod\garrysmod\addons")
                    End If
                End If
            Catch ex As Exception
                MsgBox("Tools encountered a Exception." + vbNewLine + vbNewLine + ex.Message)
            End Try
        End If
    End Sub

    Private Sub Settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.Settings.whosonline = "True" Then
            BunifuSwitch1.Value = True
        Else
            BunifuSwitch1.Value = False
        End If
        If ChkUpd.UpdateCheck.IsBusy = True Then
            Button3.Text = "Hold on!"
            Timer1.Start()
        End If
        NumericUpDown1.Value = My.Settings.threadtimer
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Process.Start("https://github.com/rondellivey03/Monolith-Launcher")
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Button3.Text = "Check" Then
            ChkUpd.Show()
            Button3.Text = "Hold on!"
            Timer1.Start()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If ChkUpd.UpdateCheck.IsBusy = True Then
            Button3.Text = "Hold on!"
        Else
            Button3.Text = "Check"
        End If
    End Sub
    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Process.Start("https://steamcommunity.com/sharedfiles/filedetails/?id=1861050128")
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        My.Settings.threadtimer = NumericUpDown1.Value
        My.Settings.Save()
        MsgBox("Value updated!")
    End Sub
    Private Sub Settings_Closed(sender As Object, e As EventArgs) Handles Me.Closed

    End Sub

    Private Sub Panel5_Paint(sender As Object, e As PaintEventArgs) Handles Panel5.Paint

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Usure.Show()
        Usure.Dock = DockStyle.Fill
        Beep()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If MLauncher.Visible = True Then
            Me.Close()
        Else
            MLauncher.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Usure.Hide()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If BunifuSwitch1.Value = True Then
            My.Settings.whosonline = "True"
            My.Settings.Save()
        ElseIf BunifuSwitch1.Value = False Then
            My.Settings.whosonline = "False"
            My.Settings.Save()
        End If
        MLauncher.Button9.PerformClick()
        Me.Close()
    End Sub
End Class
