﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MLauncher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Animation1 As BunifuAnimatorNS.Animation = New BunifuAnimatorNS.Animation()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MLauncher))
        Me.RoundEdge = New Bunifu.Framework.UI.BunifuElipse(Me.components)
        Me.HeaderPane = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.MiniLink = New System.Windows.Forms.LinkLabel()
        Me.XLink = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DragProperty = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.DragProperty2 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PROCESSCHK = New System.Windows.Forms.Timer(Me.components)
        Me.TrayIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MonolithLauncherToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Version1020000ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BunifuTransition1 = New BunifuAnimatorNS.BunifuTransition(Me.components)
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.release = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.whatsnew = New Bunifu.Framework.UI.BunifuCards()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.BunifuCustomLabel15 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BunifuCustomLabel9 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel10 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel7 = New Bunifu.Framework.UI.BunifuCards()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.BunifuCustomLabel7 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel8 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ErrCard = New Bunifu.Framework.UI.BunifuCards()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.BunifuCustomLabel6 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.ErrDes = New System.Windows.Forms.RichTextBox()
        Me.BunifuCustomLabel5 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.GameInfo = New System.Windows.Forms.Label()
        Me.LinkLabel17 = New System.Windows.Forms.LinkLabel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BunifuSeparator4 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.LinkLabel11 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.LinkLabel12 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel14 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel13 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.BunifuCustomLabel4 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.LinkLabel9 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel15 = New System.Windows.Forms.LinkLabel()
        Me.BunifuCustomLabel2 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.LinkLabel16 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel10 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel7 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel6 = New System.Windows.Forms.LinkLabel()
        Me.BunifuCustomLabel1 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.BunifuFlatButton4 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuFlatButton3 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuFlatButton2 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuFlatButton1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.BunifuSeparator2 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.BunifuSeparator1 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.BunifuCustomLabel11 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.BunifuCustomLabel12 = New Bunifu.Framework.UI.BunifuCustomLabel()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.HeaderPane.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.release.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.whatsnew.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.ErrCard.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'RoundEdge
        '
        Me.RoundEdge.ElipseRadius = 10
        Me.RoundEdge.TargetControl = Me
        '
        'HeaderPane
        '
        Me.HeaderPane.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.HeaderPane.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.HeaderPane.Controls.Add(Me.Label7)
        Me.HeaderPane.Controls.Add(Me.MiniLink)
        Me.HeaderPane.Controls.Add(Me.XLink)
        Me.HeaderPane.Controls.Add(Me.Label3)
        Me.BunifuTransition1.SetDecoration(Me.HeaderPane, BunifuAnimatorNS.DecorationType.None)
        Me.HeaderPane.Location = New System.Drawing.Point(0, 0)
        Me.HeaderPane.Name = "HeaderPane"
        Me.HeaderPane.Size = New System.Drawing.Size(1315, 53)
        Me.HeaderPane.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.Label7, BunifuAnimatorNS.DecorationType.None)
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(12, 2)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(176, 30)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Monolith Launcher"
        '
        'MiniLink
        '
        Me.MiniLink.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MiniLink.AutoSize = True
        Me.MiniLink.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.MiniLink, BunifuAnimatorNS.DecorationType.None)
        Me.MiniLink.Font = New System.Drawing.Font("Segoe UI Semibold", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MiniLink.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.MiniLink.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.MiniLink.LinkColor = System.Drawing.Color.White
        Me.MiniLink.Location = New System.Drawing.Point(1241, 7)
        Me.MiniLink.Name = "MiniLink"
        Me.MiniLink.Size = New System.Drawing.Size(31, 30)
        Me.MiniLink.TabIndex = 2
        Me.MiniLink.TabStop = True
        Me.MiniLink.Text = "__"
        '
        'XLink
        '
        Me.XLink.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XLink.AutoSize = True
        Me.XLink.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.XLink, BunifuAnimatorNS.DecorationType.None)
        Me.XLink.Font = New System.Drawing.Font("Segoe UI Semibold", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XLink.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.XLink.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.XLink.LinkColor = System.Drawing.Color.White
        Me.XLink.Location = New System.Drawing.Point(1277, 9)
        Me.XLink.Name = "XLink"
        Me.XLink.Size = New System.Drawing.Size(26, 30)
        Me.XLink.TabIndex = 1
        Me.XLink.TabStop = True
        Me.XLink.Text = "X"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.Label3, BunifuAnimatorNS.DecorationType.None)
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Light", 10.75!)
        Me.Label3.ForeColor = System.Drawing.Color.LightGray
        Me.Label3.Location = New System.Drawing.Point(15, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 20)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Version X.X.X.X"
        '
        'DragProperty
        '
        Me.DragProperty.Fixed = True
        Me.DragProperty.Horizontal = True
        Me.DragProperty.TargetControl = Me.HeaderPane
        Me.DragProperty.Vertical = True
        '
        'DragProperty2
        '
        Me.DragProperty2.Fixed = True
        Me.DragProperty2.Horizontal = True
        Me.DragProperty2.TargetControl = Me.Label7
        Me.DragProperty2.Vertical = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel1, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel1.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel1.Location = New System.Drawing.Point(1588, 644)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(160, 15)
        Me.LinkLabel1.TabIndex = 2
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "View application information"
        '
        'PROCESSCHK
        '
        Me.PROCESSCHK.Enabled = True
        Me.PROCESSCHK.Interval = 1000
        '
        'TrayIcon
        '
        Me.TrayIcon.ContextMenuStrip = Me.ContextMenuStrip1
        Me.TrayIcon.Icon = CType(resources.GetObject("TrayIcon.Icon"), System.Drawing.Icon)
        Me.TrayIcon.Text = "Monolith Launcher"
        Me.TrayIcon.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.BunifuTransition1.SetDecoration(Me.ContextMenuStrip1, BunifuAnimatorNS.DecorationType.None)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MonolithLauncherToolStripMenuItem, Me.Version1020000ToolStripMenuItem, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(176, 76)
        '
        'MonolithLauncherToolStripMenuItem
        '
        Me.MonolithLauncherToolStripMenuItem.Enabled = False
        Me.MonolithLauncherToolStripMenuItem.Name = "MonolithLauncherToolStripMenuItem"
        Me.MonolithLauncherToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.MonolithLauncherToolStripMenuItem.Text = "Monolith Launcher"
        '
        'Version1020000ToolStripMenuItem
        '
        Me.Version1020000ToolStripMenuItem.Enabled = False
        Me.Version1020000ToolStripMenuItem.Name = "Version1020000ToolStripMenuItem"
        Me.Version1020000ToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.Version1020000ToolStripMenuItem.Text = "Version 1.0.2.0000"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(172, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'BunifuTransition1
        '
        Me.BunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.Transparent
        Me.BunifuTransition1.Cursor = Nothing
        Animation1.AnimateOnlyDifferences = True
        Animation1.BlindCoeff = CType(resources.GetObject("Animation1.BlindCoeff"), System.Drawing.PointF)
        Animation1.LeafCoeff = 0!
        Animation1.MaxTime = 1.0!
        Animation1.MinTime = 0!
        Animation1.MosaicCoeff = CType(resources.GetObject("Animation1.MosaicCoeff"), System.Drawing.PointF)
        Animation1.MosaicShift = CType(resources.GetObject("Animation1.MosaicShift"), System.Drawing.PointF)
        Animation1.MosaicSize = 0
        Animation1.Padding = New System.Windows.Forms.Padding(0)
        Animation1.RotateCoeff = 0!
        Animation1.RotateLimit = 0!
        Animation1.ScaleCoeff = CType(resources.GetObject("Animation1.ScaleCoeff"), System.Drawing.PointF)
        Animation1.SlideCoeff = CType(resources.GetObject("Animation1.SlideCoeff"), System.Drawing.PointF)
        Animation1.TimeCoeff = 0!
        Animation1.TransparencyCoeff = 1.0!
        Me.BunifuTransition1.DefaultAnimation = Animation1
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.Panel11.Controls.Add(Me.release)
        Me.BunifuTransition1.SetDecoration(Me.Panel11, BunifuAnimatorNS.DecorationType.None)
        Me.Panel11.Location = New System.Drawing.Point(0, 51)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(1315, 755)
        Me.Panel11.TabIndex = 13
        '
        'release
        '
        Me.release.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.release.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.loading_bg_dark
        Me.release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.release.Controls.Add(Me.Panel4)
        Me.release.Controls.Add(Me.Panel8)
        Me.release.Controls.Add(Me.Panel3)
        Me.BunifuTransition1.SetDecoration(Me.release, BunifuAnimatorNS.DecorationType.None)
        Me.release.Dock = System.Windows.Forms.DockStyle.Fill
        Me.release.Location = New System.Drawing.Point(0, 0)
        Me.release.Name = "release"
        Me.release.Size = New System.Drawing.Size(1315, 755)
        Me.release.TabIndex = 11
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.whatsnew)
        Me.Panel4.Controls.Add(Me.PictureBox7)
        Me.Panel4.Controls.Add(Me.PictureBox12)
        Me.Panel4.Controls.Add(Me.PictureBox6)
        Me.BunifuTransition1.SetDecoration(Me.Panel4, BunifuAnimatorNS.DecorationType.None)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(638, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(674, 755)
        Me.Panel4.TabIndex = 39
        '
        'whatsnew
        '
        Me.whatsnew.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.whatsnew.BorderRadius = 15
        Me.whatsnew.BottomSahddow = True
        Me.whatsnew.color = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.whatsnew.Controls.Add(Me.Panel2)
        Me.whatsnew.Controls.Add(Me.BunifuCustomLabel15)
        Me.whatsnew.Controls.Add(Me.Button8)
        Me.whatsnew.Controls.Add(Me.Panel1)
        Me.whatsnew.Controls.Add(Me.BunifuCustomLabel9)
        Me.whatsnew.Controls.Add(Me.BunifuCustomLabel10)
        Me.BunifuTransition1.SetDecoration(Me.whatsnew, BunifuAnimatorNS.DecorationType.None)
        Me.whatsnew.LeftSahddow = False
        Me.whatsnew.Location = New System.Drawing.Point(105, 88)
        Me.whatsnew.Name = "whatsnew"
        Me.whatsnew.RightSahddow = True
        Me.whatsnew.ShadowDepth = 20
        Me.whatsnew.Size = New System.Drawing.Size(465, 578)
        Me.whatsnew.TabIndex = 25
        Me.whatsnew.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Button3)
        Me.BunifuTransition1.SetDecoration(Me.Panel2, BunifuAnimatorNS.DecorationType.None)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 522)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(465, 56)
        Me.Panel2.TabIndex = 23
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Button3.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.borders
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuTransition1.SetDecoration(Me.Button3, BunifuAnimatorNS.DecorationType.None)
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(310, 13)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(133, 30)
        Me.Button3.TabIndex = 38
        Me.Button3.Text = "Acknowledge"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'BunifuCustomLabel15
        '
        Me.BunifuCustomLabel15.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel15, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel15.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BunifuCustomLabel15.Location = New System.Drawing.Point(814, 99)
        Me.BunifuCustomLabel15.Name = "BunifuCustomLabel15"
        Me.BunifuCustomLabel15.Size = New System.Drawing.Size(271, 31)
        Me.BunifuCustomLabel15.TabIndex = 14
        Me.BunifuCustomLabel15.Text = "MLauncher Version 2.1.0.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Button8
        '
        Me.Button8.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.borders
        Me.Button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.Button8, BunifuAnimatorNS.DecorationType.None)
        Me.Button8.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button8.FlatAppearance.BorderSize = 0
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(1075, 519)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(10, 44)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Acknowledge Change-Log"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel11)
        Me.Panel1.Controls.Add(Me.RichTextBox3)
        Me.Panel1.Controls.Add(Me.BunifuSeparator1)
        Me.Panel1.Controls.Add(Me.BunifuCustomLabel12)
        Me.Panel1.Controls.Add(Me.BunifuSeparator2)
        Me.Panel1.Controls.Add(Me.RichTextBox1)
        Me.BunifuTransition1.SetDecoration(Me.Panel1, BunifuAnimatorNS.DecorationType.None)
        Me.Panel1.Location = New System.Drawing.Point(4, 67)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(456, 440)
        Me.Panel1.TabIndex = 22
        '
        'BunifuCustomLabel9
        '
        Me.BunifuCustomLabel9.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel9, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel9.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!)
        Me.BunifuCustomLabel9.ForeColor = System.Drawing.Color.Silver
        Me.BunifuCustomLabel9.Location = New System.Drawing.Point(28, 40)
        Me.BunifuCustomLabel9.Name = "BunifuCustomLabel9"
        Me.BunifuCustomLabel9.Size = New System.Drawing.Size(161, 21)
        Me.BunifuCustomLabel9.TabIndex = 10
        Me.BunifuCustomLabel9.Text = "2.1.0.5 - Nov. 2nd 2019"
        '
        'BunifuCustomLabel10
        '
        Me.BunifuCustomLabel10.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel10, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel10.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel10.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel10.Location = New System.Drawing.Point(19, 12)
        Me.BunifuCustomLabel10.Name = "BunifuCustomLabel10"
        Me.BunifuCustomLabel10.Size = New System.Drawing.Size(333, 31)
        Me.BunifuCustomLabel10.TabIndex = 7
        Me.BunifuCustomLabel10.Text = "Monolith Launcher, What's new?"
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuTransition1.SetDecoration(Me.PictureBox7, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox7.Image = Global.Monolith_Launcher.My.Resources.Resources.monoford_transp2
        Me.PictureBox7.Location = New System.Drawing.Point(89, 26)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(497, 90)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox7.TabIndex = 36
        Me.PictureBox7.TabStop = False
        Me.PictureBox7.Visible = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources._1f339
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BunifuTransition1.SetDecoration(Me.PictureBox12, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox12.Location = New System.Drawing.Point(639, 720)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox12.TabIndex = 32
        Me.PictureBox12.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuTransition1.SetDecoration(Me.PictureBox6, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PictureBox6.Image = Global.Monolith_Launcher.My.Resources.Resources.monoford_transp
        Me.PictureBox6.Location = New System.Drawing.Point(0, 131)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(674, 624)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 35
        Me.PictureBox6.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel8.Controls.Add(Me.Panel7)
        Me.Panel8.Controls.Add(Me.ErrCard)
        Me.Panel8.Controls.Add(Me.GameInfo)
        Me.Panel8.Controls.Add(Me.LinkLabel17)
        Me.Panel8.Controls.Add(Me.Panel9)
        Me.Panel8.Controls.Add(Me.LinkLabel11)
        Me.Panel8.Controls.Add(Me.LinkLabel8)
        Me.Panel8.Controls.Add(Me.PictureBox11)
        Me.Panel8.Controls.Add(Me.LinkLabel12)
        Me.Panel8.Controls.Add(Me.LinkLabel14)
        Me.Panel8.Controls.Add(Me.LinkLabel13)
        Me.Panel8.Controls.Add(Me.PictureBox9)
        Me.Panel8.Controls.Add(Me.BunifuCustomLabel4)
        Me.Panel8.Controls.Add(Me.LinkLabel9)
        Me.Panel8.Controls.Add(Me.LinkLabel15)
        Me.Panel8.Controls.Add(Me.BunifuCustomLabel2)
        Me.Panel8.Controls.Add(Me.LinkLabel16)
        Me.Panel8.Controls.Add(Me.LinkLabel10)
        Me.Panel8.Controls.Add(Me.LinkLabel7)
        Me.Panel8.Controls.Add(Me.PictureBox8)
        Me.Panel8.Controls.Add(Me.LinkLabel5)
        Me.Panel8.Controls.Add(Me.LinkLabel6)
        Me.Panel8.Controls.Add(Me.BunifuCustomLabel1)
        Me.Panel8.Controls.Add(Me.PictureBox5)
        Me.BunifuTransition1.SetDecoration(Me.Panel8, BunifuAnimatorNS.DecorationType.None)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel8.Location = New System.Drawing.Point(51, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(587, 755)
        Me.Panel8.TabIndex = 34
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.Panel7.BorderRadius = 5
        Me.Panel7.BottomSahddow = True
        Me.Panel7.color = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.Panel7.Controls.Add(Me.Button4)
        Me.Panel7.Controls.Add(Me.BunifuCustomLabel7)
        Me.Panel7.Controls.Add(Me.BunifuCustomLabel8)
        Me.BunifuTransition1.SetDecoration(Me.Panel7, BunifuAnimatorNS.DecorationType.None)
        Me.Panel7.LeftSahddow = False
        Me.Panel7.Location = New System.Drawing.Point(524, 61)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.RightSahddow = True
        Me.Panel7.ShadowDepth = 20
        Me.Panel7.Size = New System.Drawing.Size(57, 47)
        Me.Panel7.TabIndex = 28
        Me.Panel7.Visible = False
        '
        'Button4
        '
        Me.Button4.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.borders
        Me.Button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.Button4, BunifuAnimatorNS.DecorationType.None)
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(393, 688)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(170, 47)
        Me.Button4.TabIndex = 11
        Me.Button4.Text = "Retry Again"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'BunifuCustomLabel7
        '
        Me.BunifuCustomLabel7.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel7, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel7.Font = New System.Drawing.Font("Segoe UI Semilight", 15.0!)
        Me.BunifuCustomLabel7.ForeColor = System.Drawing.Color.Silver
        Me.BunifuCustomLabel7.Location = New System.Drawing.Point(59, 277)
        Me.BunifuCustomLabel7.Name = "BunifuCustomLabel7"
        Me.BunifuCustomLabel7.Size = New System.Drawing.Size(469, 56)
        Me.BunifuCustomLabel7.TabIndex = 10
        Me.BunifuCustomLabel7.Text = "Something went wrong while trying to connect online, " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "check your network connect" &
    "ion then try again. "
        Me.BunifuCustomLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'BunifuCustomLabel8
        '
        Me.BunifuCustomLabel8.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel8, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel8.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel8.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel8.Location = New System.Drawing.Point(124, 235)
        Me.BunifuCustomLabel8.Name = "BunifuCustomLabel8"
        Me.BunifuCustomLabel8.Size = New System.Drawing.Size(338, 31)
        Me.BunifuCustomLabel8.TabIndex = 7
        Me.BunifuCustomLabel8.Text = "Couldn't establish an connection."
        '
        'ErrCard
        '
        Me.ErrCard.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ErrCard.BorderRadius = 5
        Me.ErrCard.BottomSahddow = True
        Me.ErrCard.color = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ErrCard.Controls.Add(Me.Button1)
        Me.ErrCard.Controls.Add(Me.Button6)
        Me.ErrCard.Controls.Add(Me.Button7)
        Me.ErrCard.Controls.Add(Me.BunifuCustomLabel6)
        Me.ErrCard.Controls.Add(Me.ErrDes)
        Me.ErrCard.Controls.Add(Me.BunifuCustomLabel5)
        Me.BunifuTransition1.SetDecoration(Me.ErrCard, BunifuAnimatorNS.DecorationType.None)
        Me.ErrCard.LeftSahddow = False
        Me.ErrCard.Location = New System.Drawing.Point(525, 8)
        Me.ErrCard.Name = "ErrCard"
        Me.ErrCard.RightSahddow = True
        Me.ErrCard.ShadowDepth = 20
        Me.ErrCard.Size = New System.Drawing.Size(56, 47)
        Me.ErrCard.TabIndex = 2
        Me.ErrCard.Visible = False
        '
        'Button1
        '
        Me.Button1.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.borders
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.Button1, BunifuAnimatorNS.DecorationType.None)
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(193, 668)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 59)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Create new Github Issue"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.BackgroundImage = CType(resources.GetObject("Button6.BackgroundImage"), System.Drawing.Image)
        Me.Button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.Button6, BunifuAnimatorNS.DecorationType.None)
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Button6.Location = New System.Drawing.Point(322, 668)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(114, 59)
        Me.Button6.TabIndex = 12
        Me.Button6.Text = "Exit App"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.BackgroundImage = CType(resources.GetObject("Button7.BackgroundImage"), System.Drawing.Image)
        Me.Button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.Button7, BunifuAnimatorNS.DecorationType.None)
        Me.Button7.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Button7.FlatAppearance.BorderSize = 0
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Button7.Location = New System.Drawing.Point(442, 668)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(114, 59)
        Me.Button7.TabIndex = 11
        Me.Button7.Text = "Acknowledge"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'BunifuCustomLabel6
        '
        Me.BunifuCustomLabel6.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel6, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel6.Font = New System.Drawing.Font("Segoe UI Semilight", 15.0!)
        Me.BunifuCustomLabel6.ForeColor = System.Drawing.Color.Silver
        Me.BunifuCustomLabel6.Location = New System.Drawing.Point(34, 50)
        Me.BunifuCustomLabel6.Name = "BunifuCustomLabel6"
        Me.BunifuCustomLabel6.Size = New System.Drawing.Size(317, 28)
        Me.BunifuCustomLabel6.TabIndex = 10
        Me.BunifuCustomLabel6.Text = "Below is information about the error."
        '
        'ErrDes
        '
        Me.ErrDes.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ErrDes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.BunifuTransition1.SetDecoration(Me.ErrDes, BunifuAnimatorNS.DecorationType.None)
        Me.ErrDes.Font = New System.Drawing.Font("Segoe UI Semilight", 13.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ErrDes.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ErrDes.Location = New System.Drawing.Point(39, 122)
        Me.ErrDes.Name = "ErrDes"
        Me.ErrDes.ReadOnly = True
        Me.ErrDes.Size = New System.Drawing.Size(514, 370)
        Me.ErrDes.TabIndex = 8
        Me.ErrDes.Text = ""
        '
        'BunifuCustomLabel5
        '
        Me.BunifuCustomLabel5.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel5, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel5.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel5.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel5.Location = New System.Drawing.Point(33, 19)
        Me.BunifuCustomLabel5.Name = "BunifuCustomLabel5"
        Me.BunifuCustomLabel5.Size = New System.Drawing.Size(367, 31)
        Me.BunifuCustomLabel5.TabIndex = 7
        Me.BunifuCustomLabel5.Text = "The Launcher ran across a problem."
        '
        'GameInfo
        '
        Me.GameInfo.AutoSize = True
        Me.GameInfo.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.GameInfo, BunifuAnimatorNS.DecorationType.None)
        Me.GameInfo.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.GameInfo.ForeColor = System.Drawing.Color.Lime
        Me.GameInfo.Location = New System.Drawing.Point(199, 528)
        Me.GameInfo.Name = "GameInfo"
        Me.GameInfo.Size = New System.Drawing.Size(188, 19)
        Me.GameInfo.TabIndex = 31
        Me.GameInfo.Text = "An HL2.exe process is active."
        '
        'LinkLabel17
        '
        Me.LinkLabel17.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel17, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel17.ForeColor = System.Drawing.Color.Black
        Me.LinkLabel17.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel17.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel17.Location = New System.Drawing.Point(439, 688)
        Me.LinkLabel17.Name = "LinkLabel17"
        Me.LinkLabel17.Size = New System.Drawing.Size(85, 15)
        Me.LinkLabel17.TabIndex = 17
        Me.LinkLabel17.TabStop = True
        Me.LinkLabel17.Text = "File an appeal"
        Me.LinkLabel17.UseMnemonic = False
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.LinkLabel3)
        Me.Panel9.Controls.Add(Me.Label6)
        Me.Panel9.Controls.Add(Me.RichTextBox2)
        Me.Panel9.Controls.Add(Me.Label5)
        Me.Panel9.Controls.Add(Me.Label4)
        Me.Panel9.Controls.Add(Me.BunifuSeparator4)
        Me.BunifuTransition1.SetDecoration(Me.Panel9, BunifuAnimatorNS.DecorationType.None)
        Me.Panel9.Location = New System.Drawing.Point(139, 170)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(308, 313)
        Me.Panel9.TabIndex = 23
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel3, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel3.LinkColor = System.Drawing.Color.White
        Me.LinkLabel3.Location = New System.Drawing.Point(210, 285)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(50, 15)
        Me.LinkLabel3.TabIndex = 17
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Refresh"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.Label6, BunifuAnimatorNS.DecorationType.None)
        Me.Label6.ForeColor = System.Drawing.Color.DimGray
        Me.Label6.Location = New System.Drawing.Point(46, 285)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(158, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Info provided by GMOD-Servers"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.RichTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.BunifuTransition1.SetDecoration(Me.RichTextBox2, BunifuAnimatorNS.DecorationType.None)
        Me.RichTextBox2.Font = New System.Drawing.Font("Segoe UI Emoji", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.ForeColor = System.Drawing.Color.White
        Me.RichTextBox2.Location = New System.Drawing.Point(21, 106)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(264, 176)
        Me.RichTextBox2.TabIndex = 32
        Me.RichTextBox2.Text = "Loading Whos Online"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.Label5, BunifuAnimatorNS.DecorationType.None)
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Light", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Info
        Me.Label5.Location = New System.Drawing.Point(70, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(167, 34)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "See whos currently playing " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Monolith Roleplay right now!"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.Label4, BunifuAnimatorNS.DecorationType.None)
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(69, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(164, 32)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Whos Online?"
        '
        'BunifuSeparator4
        '
        Me.BunifuSeparator4.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuSeparator4, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator4.LineThickness = 1
        Me.BunifuSeparator4.Location = New System.Drawing.Point(21, 79)
        Me.BunifuSeparator4.Name = "BunifuSeparator4"
        Me.BunifuSeparator4.Size = New System.Drawing.Size(264, 35)
        Me.BunifuSeparator4.TabIndex = 33
        Me.BunifuSeparator4.Transparency = 255
        Me.BunifuSeparator4.Vertical = False
        '
        'LinkLabel11
        '
        Me.LinkLabel11.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel11, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel11.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel11.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel11.Location = New System.Drawing.Point(79, 706)
        Me.LinkLabel11.Name = "LinkLabel11"
        Me.LinkLabel11.Size = New System.Drawing.Size(104, 15)
        Me.LinkLabel11.TabIndex = 16
        Me.LinkLabel11.TabStop = True
        Me.LinkLabel11.Text = "Monolith Updates"
        Me.LinkLabel11.UseMnemonic = False
        '
        'LinkLabel8
        '
        Me.LinkLabel8.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel8, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel8.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel8.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel8.Location = New System.Drawing.Point(258, 668)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(131, 15)
        Me.LinkLabel8.TabIndex = 16
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "Monolith Steam Group"
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.icons8_headset_50
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BunifuTransition1.SetDecoration(Me.PictureBox11, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox11.Location = New System.Drawing.Point(393, 596)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(39, 46)
        Me.PictureBox11.TabIndex = 11
        Me.PictureBox11.TabStop = False
        '
        'LinkLabel12
        '
        Me.LinkLabel12.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel12, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel12.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel12.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel12.Location = New System.Drawing.Point(439, 711)
        Me.LinkLabel12.Name = "LinkLabel12"
        Me.LinkLabel12.Size = New System.Drawing.Size(73, 15)
        Me.LinkLabel12.TabIndex = 15
        Me.LinkLabel12.TabStop = True
        Me.LinkLabel12.Text = "Rules & Info"
        Me.LinkLabel12.UseMnemonic = False
        '
        'LinkLabel14
        '
        Me.LinkLabel14.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel14, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel14.ForeColor = System.Drawing.Color.Black
        Me.LinkLabel14.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel14.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel14.Location = New System.Drawing.Point(439, 667)
        Me.LinkLabel14.Name = "LinkLabel14"
        Me.LinkLabel14.Size = New System.Drawing.Size(77, 15)
        Me.LinkLabel14.TabIndex = 16
        Me.LinkLabel14.TabStop = True
        Me.LinkLabel14.Text = "Staff Reports"
        Me.LinkLabel14.UseMnemonic = False
        '
        'LinkLabel13
        '
        Me.LinkLabel13.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel13, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel13.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel13.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel13.Location = New System.Drawing.Point(79, 683)
        Me.LinkLabel13.Name = "LinkLabel13"
        Me.LinkLabel13.Size = New System.Drawing.Size(96, 15)
        Me.LinkLabel13.TabIndex = 14
        Me.LinkLabel13.TabStop = True
        Me.LinkLabel13.Text = "Announcements"
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.icons8_share_50
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BunifuTransition1.SetDecoration(Me.PictureBox9, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox9.Location = New System.Drawing.Point(211, 596)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(39, 46)
        Me.PictureBox9.TabIndex = 11
        Me.PictureBox9.TabStop = False
        '
        'BunifuCustomLabel4
        '
        Me.BunifuCustomLabel4.AutoSize = True
        Me.BunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel4, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel4.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel4.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel4.Location = New System.Drawing.Point(438, 596)
        Me.BunifuCustomLabel4.Name = "BunifuCustomLabel4"
        Me.BunifuCustomLabel4.Size = New System.Drawing.Size(115, 20)
        Me.BunifuCustomLabel4.TabIndex = 12
        Me.BunifuCustomLabel4.Text = "Support & Help"
        Me.BunifuCustomLabel4.UseMnemonic = False
        '
        'LinkLabel9
        '
        Me.LinkLabel9.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel9, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel9.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel9.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel9.Location = New System.Drawing.Point(258, 645)
        Me.LinkLabel9.Name = "LinkLabel9"
        Me.LinkLabel9.Size = New System.Drawing.Size(109, 15)
        Me.LinkLabel9.TabIndex = 15
        Me.LinkLabel9.TabStop = True
        Me.LinkLabel9.Text = "MonoRust Discord"
        '
        'LinkLabel15
        '
        Me.LinkLabel15.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel15, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel15.ForeColor = System.Drawing.Color.Black
        Me.LinkLabel15.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel15.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel15.Location = New System.Drawing.Point(439, 643)
        Me.LinkLabel15.Name = "LinkLabel15"
        Me.LinkLabel15.Size = New System.Drawing.Size(87, 15)
        Me.LinkLabel15.TabIndex = 15
        Me.LinkLabel15.TabStop = True
        Me.LinkLabel15.Text = "Player Reports"
        Me.LinkLabel15.UseMnemonic = False
        '
        'BunifuCustomLabel2
        '
        Me.BunifuCustomLabel2.AutoSize = True
        Me.BunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel2.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel2.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel2.Location = New System.Drawing.Point(256, 596)
        Me.BunifuCustomLabel2.Name = "BunifuCustomLabel2"
        Me.BunifuCustomLabel2.Size = New System.Drawing.Size(49, 20)
        Me.BunifuCustomLabel2.TabIndex = 12
        Me.BunifuCustomLabel2.Text = "Social"
        '
        'LinkLabel16
        '
        Me.LinkLabel16.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel16, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel16.ForeColor = System.Drawing.Color.Black
        Me.LinkLabel16.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel16.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel16.Location = New System.Drawing.Point(439, 621)
        Me.LinkLabel16.Name = "LinkLabel16"
        Me.LinkLabel16.Size = New System.Drawing.Size(102, 15)
        Me.LinkLabel16.TabIndex = 14
        Me.LinkLabel16.TabStop = True
        Me.LinkLabel16.Text = "In-game Refunds"
        '
        'LinkLabel10
        '
        Me.LinkLabel10.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel10, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel10.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel10.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel10.Location = New System.Drawing.Point(258, 622)
        Me.LinkLabel10.Name = "LinkLabel10"
        Me.LinkLabel10.Size = New System.Drawing.Size(117, 15)
        Me.LinkLabel10.TabIndex = 14
        Me.LinkLabel10.TabStop = True
        Me.LinkLabel10.Text = "MonolithRP Discord"
        '
        'LinkLabel7
        '
        Me.LinkLabel7.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel7, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LinkLabel7.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel7.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel7.Location = New System.Drawing.Point(79, 662)
        Me.LinkLabel7.Name = "LinkLabel7"
        Me.LinkLabel7.Size = New System.Drawing.Size(77, 15)
        Me.LinkLabel7.TabIndex = 16
        Me.LinkLabel7.TabStop = True
        Me.LinkLabel7.Text = "View Servers"
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox8.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.logo_2_
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BunifuTransition1.SetDecoration(Me.PictureBox8, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox8.Location = New System.Drawing.Point(33, 596)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(39, 46)
        Me.PictureBox8.TabIndex = 11
        Me.PictureBox8.TabStop = False
        '
        'LinkLabel5
        '
        Me.LinkLabel5.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel5, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LinkLabel5.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel5.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel5.Location = New System.Drawing.Point(79, 642)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(61, 15)
        Me.LinkLabel5.TabIndex = 15
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "Visit Store"
        '
        'LinkLabel6
        '
        Me.LinkLabel6.AutoSize = True
        Me.BunifuTransition1.SetDecoration(Me.LinkLabel6, BunifuAnimatorNS.DecorationType.None)
        Me.LinkLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LinkLabel6.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.LinkLabel6.LinkColor = System.Drawing.Color.DarkGray
        Me.LinkLabel6.Location = New System.Drawing.Point(79, 621)
        Me.LinkLabel6.Name = "LinkLabel6"
        Me.LinkLabel6.Size = New System.Drawing.Size(74, 15)
        Me.LinkLabel6.TabIndex = 14
        Me.LinkLabel6.TabStop = True
        Me.LinkLabel6.Text = "Visit Forums"
        '
        'BunifuCustomLabel1
        '
        Me.BunifuCustomLabel1.AutoSize = True
        Me.BunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel1.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel1.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel1.Location = New System.Drawing.Point(78, 596)
        Me.BunifuCustomLabel1.Name = "BunifuCustomLabel1"
        Me.BunifuCustomLabel1.Size = New System.Drawing.Size(127, 20)
        Me.BunifuCustomLabel1.TabIndex = 12
        Me.BunifuCustomLabel1.Text = "Monolith Servers"
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuTransition1.SetDecoration(Me.PictureBox5, BunifuAnimatorNS.DecorationType.None)
        Me.PictureBox5.Image = Global.Monolith_Launcher.My.Resources.Resources.K20GBvj
        Me.PictureBox5.Location = New System.Drawing.Point(70, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(444, 108)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 37
        Me.PictureBox5.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.BunifuFlatButton4)
        Me.Panel3.Controls.Add(Me.BunifuFlatButton3)
        Me.Panel3.Controls.Add(Me.BunifuFlatButton2)
        Me.Panel3.Controls.Add(Me.BunifuFlatButton1)
        Me.Panel3.Controls.Add(Me.Button9)
        Me.BunifuTransition1.SetDecoration(Me.Panel3, BunifuAnimatorNS.DecorationType.None)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(51, 755)
        Me.Panel3.TabIndex = 38
        '
        'BunifuFlatButton4
        '
        Me.BunifuFlatButton4.Activecolor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton4.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton4.BorderRadius = 0
        Me.BunifuFlatButton4.ButtonText = "Show Settings"
        Me.BunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.BunifuFlatButton4, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton4.Iconimage = Global.Monolith_Launcher.My.Resources.Resources.services_50px
        Me.BunifuFlatButton4.Iconimage_right = Nothing
        Me.BunifuFlatButton4.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton4.Iconimage_Selected = Nothing
        Me.BunifuFlatButton4.IconMarginLeft = 0
        Me.BunifuFlatButton4.IconMarginRight = 0
        Me.BunifuFlatButton4.IconRightVisible = True
        Me.BunifuFlatButton4.IconRightZoom = 0R
        Me.BunifuFlatButton4.IconVisible = True
        Me.BunifuFlatButton4.IconZoom = 90.0R
        Me.BunifuFlatButton4.IsTab = False
        Me.BunifuFlatButton4.Location = New System.Drawing.Point(3, 56)
        Me.BunifuFlatButton4.Name = "BunifuFlatButton4"
        Me.BunifuFlatButton4.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton4.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.BunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton4.selected = False
        Me.BunifuFlatButton4.Size = New System.Drawing.Size(241, 48)
        Me.BunifuFlatButton4.TabIndex = 40
        Me.BunifuFlatButton4.Text = "Show Settings"
        Me.BunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton4.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolTip1.SetToolTip(Me.BunifuFlatButton4, "Subscribe to addons")
        '
        'BunifuFlatButton3
        '
        Me.BunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton3.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton3.BorderRadius = 0
        Me.BunifuFlatButton3.ButtonText = "Subscribe to addons"
        Me.BunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.BunifuFlatButton3, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton3.Iconimage = Global.Monolith_Launcher.My.Resources.Resources.plugin_50px
        Me.BunifuFlatButton3.Iconimage_right = Nothing
        Me.BunifuFlatButton3.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton3.Iconimage_Selected = Nothing
        Me.BunifuFlatButton3.IconMarginLeft = 0
        Me.BunifuFlatButton3.IconMarginRight = 0
        Me.BunifuFlatButton3.IconRightVisible = True
        Me.BunifuFlatButton3.IconRightZoom = 0R
        Me.BunifuFlatButton3.IconVisible = True
        Me.BunifuFlatButton3.IconZoom = 90.0R
        Me.BunifuFlatButton3.IsTab = False
        Me.BunifuFlatButton3.Location = New System.Drawing.Point(2, 649)
        Me.BunifuFlatButton3.Name = "BunifuFlatButton3"
        Me.BunifuFlatButton3.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton3.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.BunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton3.selected = False
        Me.BunifuFlatButton3.Size = New System.Drawing.Size(241, 48)
        Me.BunifuFlatButton3.TabIndex = 2
        Me.BunifuFlatButton3.Text = "Subscribe to addons"
        Me.BunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton3.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolTip1.SetToolTip(Me.BunifuFlatButton3, "Subscribe to addons")
        '
        'BunifuFlatButton2
        '
        Me.BunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton2.BorderRadius = 0
        Me.BunifuFlatButton2.ButtonText = "Collapse Menu"
        Me.BunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.BunifuFlatButton2, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton2.Iconimage = Global.Monolith_Launcher.My.Resources.Resources.icons8_menu_vertical_50
        Me.BunifuFlatButton2.Iconimage_right = Nothing
        Me.BunifuFlatButton2.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton2.Iconimage_Selected = Nothing
        Me.BunifuFlatButton2.IconMarginLeft = 0
        Me.BunifuFlatButton2.IconMarginRight = 0
        Me.BunifuFlatButton2.IconRightVisible = True
        Me.BunifuFlatButton2.IconRightZoom = 0R
        Me.BunifuFlatButton2.IconVisible = True
        Me.BunifuFlatButton2.IconZoom = 90.0R
        Me.BunifuFlatButton2.IsTab = False
        Me.BunifuFlatButton2.Location = New System.Drawing.Point(2, 2)
        Me.BunifuFlatButton2.Name = "BunifuFlatButton2"
        Me.BunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton2.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.BunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton2.selected = False
        Me.BunifuFlatButton2.Size = New System.Drawing.Size(241, 48)
        Me.BunifuFlatButton2.TabIndex = 1
        Me.BunifuFlatButton2.Text = "Collapse Menu"
        Me.BunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton2.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolTip1.SetToolTip(Me.BunifuFlatButton2, "Menu")
        '
        'BunifuFlatButton1
        '
        Me.BunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton1.BorderRadius = 0
        Me.BunifuFlatButton1.ButtonText = "Launch Monolith"
        Me.BunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuTransition1.SetDecoration(Me.BunifuFlatButton1, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton1.Iconimage = Global.Monolith_Launcher.My.Resources.Resources.icons8_circled_play_50
        Me.BunifuFlatButton1.Iconimage_right = Nothing
        Me.BunifuFlatButton1.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton1.Iconimage_Selected = Nothing
        Me.BunifuFlatButton1.IconMarginLeft = 0
        Me.BunifuFlatButton1.IconMarginRight = 0
        Me.BunifuFlatButton1.IconRightVisible = True
        Me.BunifuFlatButton1.IconRightZoom = 0R
        Me.BunifuFlatButton1.IconVisible = True
        Me.BunifuFlatButton1.IconZoom = 90.0R
        Me.BunifuFlatButton1.IsTab = False
        Me.BunifuFlatButton1.Location = New System.Drawing.Point(1, 704)
        Me.BunifuFlatButton1.Name = "BunifuFlatButton1"
        Me.BunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.BunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(66, Byte), Integer))
        Me.BunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton1.selected = False
        Me.BunifuFlatButton1.Size = New System.Drawing.Size(241, 48)
        Me.BunifuFlatButton1.TabIndex = 0
        Me.BunifuFlatButton1.Text = "Launch Monolith"
        Me.BunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton1.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolTip1.SetToolTip(Me.BunifuFlatButton1, "Play Monolith")
        '
        'Button9
        '
        Me.Button9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.Button9.BackgroundImage = Global.Monolith_Launcher.My.Resources.Resources.borders
        Me.Button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuTransition1.SetDecoration(Me.Button9, BunifuAnimatorNS.DecorationType.None)
        Me.Button9.FlatAppearance.BorderSize = 0
        Me.Button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Segoe UI Semilight", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(3, 711)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(133, 30)
        Me.Button9.TabIndex = 39
        Me.Button9.Text = "Acknowledge"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Nothing
        Me.BunifuDragControl1.Vertical = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 2000
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(151, 25)
        Me.ToolStripButton1.Text = "Advanced Settings"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton2.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ToolStripButton2.Image = Global.Monolith_Launcher.My.Resources.Resources.icons8_share_50
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(178, 25)
        Me.ToolStripButton2.Text = "The Launchers Discord"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.BunifuTransition1.SetDecoration(Me.RichTextBox1, BunifuAnimatorNS.DecorationType.None)
        Me.RichTextBox1.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.ForeColor = System.Drawing.Color.Silver
        Me.RichTextBox1.Location = New System.Drawing.Point(15, 48)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(414, 48)
        Me.RichTextBox1.TabIndex = 18
        Me.RichTextBox1.Text = "I cleaned up and removed blank, and unused source codes, and also corrected posit" &
    "ions of the UI."
        '
        'BunifuSeparator2
        '
        Me.BunifuSeparator2.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuSeparator2, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuSeparator2.ForeColor = System.Drawing.Color.LimeGreen
        Me.BunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BunifuSeparator2.LineThickness = 1
        Me.BunifuSeparator2.Location = New System.Drawing.Point(17, 118)
        Me.BunifuSeparator2.Name = "BunifuSeparator2"
        Me.BunifuSeparator2.Size = New System.Drawing.Size(420, 25)
        Me.BunifuSeparator2.TabIndex = 19
        Me.BunifuSeparator2.Transparency = 255
        Me.BunifuSeparator2.Vertical = False
        '
        'BunifuSeparator1
        '
        Me.BunifuSeparator1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuSeparator1, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuSeparator1.ForeColor = System.Drawing.Color.White
        Me.BunifuSeparator1.LineColor = System.Drawing.Color.White
        Me.BunifuSeparator1.LineThickness = 1
        Me.BunifuSeparator1.Location = New System.Drawing.Point(20, 15)
        Me.BunifuSeparator1.Name = "BunifuSeparator1"
        Me.BunifuSeparator1.Size = New System.Drawing.Size(413, 25)
        Me.BunifuSeparator1.TabIndex = 12
        Me.BunifuSeparator1.Transparency = 255
        Me.BunifuSeparator1.Vertical = False
        '
        'BunifuCustomLabel11
        '
        Me.BunifuCustomLabel11.AutoSize = True
        Me.BunifuCustomLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel11, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel11.Font = New System.Drawing.Font("Segoe UI Semibold", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel11.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel11.Location = New System.Drawing.Point(14, 15)
        Me.BunifuCustomLabel11.Name = "BunifuCustomLabel11"
        Me.BunifuCustomLabel11.Size = New System.Drawing.Size(153, 25)
        Me.BunifuCustomLabel11.TabIndex = 17
        Me.BunifuCustomLabel11.Text = "Source Cleanup?"
        '
        'BunifuCustomLabel12
        '
        Me.BunifuCustomLabel12.AutoSize = True
        Me.BunifuCustomLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BunifuTransition1.SetDecoration(Me.BunifuCustomLabel12, BunifuAnimatorNS.DecorationType.None)
        Me.BunifuCustomLabel12.Font = New System.Drawing.Font("Segoe UI Semibold", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuCustomLabel12.ForeColor = System.Drawing.Color.White
        Me.BunifuCustomLabel12.Location = New System.Drawing.Point(10, 114)
        Me.BunifuCustomLabel12.Name = "BunifuCustomLabel12"
        Me.BunifuCustomLabel12.Size = New System.Drawing.Size(219, 25)
        Me.BunifuCustomLabel12.TabIndex = 20
        Me.BunifuCustomLabel12.Text = "Monolith Launcher 2 !!!!"
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(43, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.RichTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.BunifuTransition1.SetDecoration(Me.RichTextBox3, BunifuAnimatorNS.DecorationType.None)
        Me.RichTextBox3.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox3.ForeColor = System.Drawing.Color.Silver
        Me.RichTextBox3.Location = New System.Drawing.Point(8, 149)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.ReadOnly = True
        Me.RichTextBox3.Size = New System.Drawing.Size(430, 332)
        Me.RichTextBox3.TabIndex = 21
        Me.RichTextBox3.Text = resources.GetString("RichTextBox3.Text")
        '
        'MLauncher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1315, 807)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.HeaderPane)
        Me.Controls.Add(Me.LinkLabel1)
        Me.BunifuTransition1.SetDecoration(Me, BunifuAnimatorNS.DecorationType.None)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MLauncher"
        Me.Text = "Monolith Launcher by Rondell"
        Me.HeaderPane.ResumeLayout(False)
        Me.HeaderPane.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.release.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.whatsnew.ResumeLayout(False)
        Me.whatsnew.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.ErrCard.ResumeLayout(False)
        Me.ErrCard.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RoundEdge As Bunifu.Framework.UI.BunifuElipse
    Friend WithEvents HeaderPane As Panel
    Friend WithEvents MiniLink As LinkLabel
    Friend WithEvents XLink As LinkLabel
    Friend WithEvents DragProperty As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents DragProperty2 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents GameInfoLabel As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PROCESSCHK As Timer
    Friend WithEvents release As Panel
    Friend WithEvents TrayIcon As NotifyIcon
    Friend WithEvents BunifuTransition1 As BunifuAnimatorNS.BunifuTransition
    Friend WithEvents BunifuCustomLabel1 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents LinkLabel6 As LinkLabel
    Friend WithEvents LinkLabel5 As LinkLabel
    Friend WithEvents LinkLabel7 As LinkLabel
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents LinkLabel8 As LinkLabel
    Friend WithEvents BunifuCustomLabel2 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents LinkLabel9 As LinkLabel
    Friend WithEvents LinkLabel10 As LinkLabel
    Friend WithEvents LinkLabel11 As LinkLabel
    Friend WithEvents LinkLabel12 As LinkLabel
    Friend WithEvents LinkLabel13 As LinkLabel
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents LinkLabel14 As LinkLabel
    Friend WithEvents BunifuCustomLabel4 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents LinkLabel15 As LinkLabel
    Friend WithEvents LinkLabel16 As LinkLabel
    Friend WithEvents LinkLabel17 As LinkLabel
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents MonolithLauncherToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Version1020000ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ErrCard As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents ErrDes As RichTextBox
    Friend WithEvents BunifuCustomLabel5 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
    Friend WithEvents BunifuCustomLabel6 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents whatsnew As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents Button8 As Button
    Friend WithEvents BunifuCustomLabel9 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel10 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Panel7 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents Button4 As Button
    Friend WithEvents BunifuCustomLabel7 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuCustomLabel8 As Bunifu.Framework.UI.BunifuCustomLabel

    Private Sub XLink_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles XLink.LinkClicked

    End Sub
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Panel9 As Panel
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents BunifuSeparator4 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label6 As Label
    Friend WithEvents LinkLabel3 As LinkLabel
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents Button1 As Button
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents Panel11 As Panel
    Friend WithEvents GameInfo As Label
    Friend WithEvents BunifuCustomLabel15 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Button3 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button9 As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents BunifuFlatButton1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton2 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton3 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel4 As Panel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents BunifuFlatButton4 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuCustomLabel11 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents RichTextBox3 As RichTextBox
    Friend WithEvents BunifuSeparator1 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents BunifuCustomLabel12 As Bunifu.Framework.UI.BunifuCustomLabel
    Friend WithEvents BunifuSeparator2 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents RichTextBox1 As RichTextBox
End Class
